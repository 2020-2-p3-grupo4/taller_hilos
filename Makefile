matmul: matriz.c
	gcc -Wall -pthread matriz.c -o matmul

.PHONY: clean
clean:
	rm matmul

run:
	./matmul --filas_m1 2000 --cols_m1 1000 --filas_m2 1000 --cols_m2 3000 --n_hilos 1 
